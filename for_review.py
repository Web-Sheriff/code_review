from library.models import *
import datetime
import re
from django.db import models
from django.utils.timezone import now


class Login(models.Model):
    username = models.CharField(max_length=64)
    password = models.CharField(max_length=64)


class Author(models.Model):
    name = models.CharField(max_length=64)


class Keyword(models.Model):
    word = models.CharField(max_length=255)


class Document(models.Model):
    library = models.ForeignKey(Library, on_delete=models.DO_NOTHING, related_name='documents')
    title = models.CharField(max_length=128)
    authors = models.ManyToManyField(Author, related_name='documents')
    price_value = models.IntegerField()
    keywords = models.ManyToManyField(Keyword, related_name='documents')
    is_best_seller = models.BooleanField(default=False)
    edition = models.CharField(max_length=128)
    publisher = models.CharField(max_length=64)
    year = models.IntegerField()
    studentsQueue = models.ManyToManyField("Student", related_name='documents')
    instructorsQueue = models.ManyToManyField("Instructor", related_name='documents')
    TAsQueue = models.ManyToManyField("TA", related_name='documents')
    visitingProfessorsQueue = models.ManyToManyField("VisitingProfessor", related_name='documents')
    professorsQueue = models.ManyToManyField("Professor", related_name='documents')

    def booking_period(self, user):
        return datetime.timedelta(weeks=2)

    def queue_type(self, doc, user):
        if isinstance(user, Student):
            return doc.studentsQueue
        elif isinstance(user, Instructor):
            return doc.instructorsQueue
        elif isinstance(user, TA):
            return doc.TAsQueue
        elif isinstance(user, VisitingProfessor):
            return doc.visitingProfessorsQueue
        else:
            return doc.professorsQueue

    def first_in_queue(self, doc):
        if doc.studentsQueue.count() > 0:
            return doc.studentsQueue.first()
        elif doc.instructorsQueue.count() > 0:
            return doc.instructorsQueue.first()
        elif doc.TAsQueue.count() > 0:
            return doc.TAsQueue.first()
        elif doc.visitingProfessorsQueue.count() > 0:
            return doc.visitingProfessorsQueue.first()
        else:
            return doc.professorsQueue.first()


class Book(Document):
    def booking_period(self, user):
        if self.is_best_seller:
            return datetime.timedelta(days=14)
        if isinstance(user, Faculty):
            return datetime.timedelta(days=28)
        if isinstance(user, VisitingProfessor):
            return datetime.timedelta(days=7)
        return datetime.timedelta(days=21)


class ReferenceBook(Book):
    authors = models.ManyToManyField(Author, related_name='reference')
    keywords = models.ManyToManyField(Keyword, related_name='reference')


class AudioVideo(Document):
    def booking_period(self, user):
        if isinstance(user, VisitingProfessor):
            return datetime.timedelta(weeks=1)
        return datetime.timedelta(weeks=2)


class Editor(models.Model):
    first_name = models.CharField(max_length=64)
    second_name = models.CharField(max_length=64)


class Journal(models.Model):
    title = models.CharField(max_length=128)
    library = models.ForeignKey(Library, on_delete=models.DO_NOTHING, related_name='journals')
    authors = models.ManyToManyField(Author, related_name='journals')
    price_value = models.IntegerField()
    keywords = models.ManyToManyField(Keyword, related_name='journals')

    def booking_period(self, user):
        if isinstance(user, VisitingProfessor):
            return datetime.timedelta(weeks=1)
        return datetime.timedelta(weeks=2)


class Issue(models.Model):
    publication_date = models.DateField()
    editors = models.ManyToManyField(Editor, related_name='issues')
    journal = models.ForeignKey(Journal, on_delete=models.DO_NOTHING, related_name='issues')


class JournalArticles(Document):
    issue = models.ForeignKey(Issue, on_delete=models.DO_NOTHING, related_name='journal_articles')


class Copy(models.Model):
    document = models.ForeignKey(Document, on_delete=models.DO_NOTHING, related_name='copies')
    number = models.IntegerField()
    is_checked_out = models.BooleanField(default=False)
    need_to_return = models.BooleanField(default=False)
    booking_date = models.DateField(null=True)
    overdue_date = models.DateField(null=True)
    renew = models.ForeignKey("Librarian", on_delete=models.DO_NOTHING, related_name='renew', null=True)
    weeks_renew = models.ForeignKey("Librarian", on_delete=models.DO_NOTHING, related_name='weeks_renew', null=True)

    def if_overdue(self):
        return datetime.now() > self.overdue_date

    def overdue(self):
        return datetime.now() - self.overdue_date


class User(models.Model):
    login = models.CharField(max_length=64)
    password = models.CharField(max_length=64)
    mail = models.EmailField(max_length=64)
    first_name = models.CharField(max_length=64)
    second_name = models.CharField(max_length=64)
    address = models.CharField(max_length=256)
    phone_number = models.CharField(max_length=16)
    fine = models.IntegerField()


class UserCard(models.Model):
    user = models.OneToOneField(User, on_delete=models.DO_NOTHING, related_name='user_card')
    library_card_number = models.CharField(max_length=128)
    library = models.ForeignKey(Library, on_delete=models.DO_NOTHING, related_name='user_cards')
    copies = models.ManyToManyField(Copy)


class AvailableDocs(models.Model):
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING, related_name='available_documents')
    document = models.OneToOneField(Document, on_delete=models.DO_NOTHING)
    rights_date = models.DateField(now())

    def check_date(self):
        if self.rights_date != datetime.date.today():
            queue = Document.queue_type(doc=self.document, user=self.user)
            queue.exclude(user_card=self.user.user_card)
            queue.model.save()
            self.user.available_documents.exclude(document=self.document, user=self.user)
            self.user.available_documents.model.save()
            first = Document.first_in_queue(doc=self.document)
            first.available_documents.create(document=self.document, user=first)
            Librarian.notify(user=first, document=self.document)


class Patron(User):

    # renew the document for n weeks
    def renew(self, queue=Copy.renew):  # 13 requirement
        queue.add(self)
        queue.models.save
        pass

    # search for the documents using string
    def search_doc(self, string):
        d1 = self.search_doc_author(string)
        d2 = self.search_doc_title(string)
        d3 = self.search_doc_keywords(string)
        d4 = d1 | d2 | d3
        return d4.distinct()

    # search for the documents using string, which is the name of the author
    def search_doc_author(self, name):
        documents = self.user_card.library.documents.filter(authors__name__contains=name).distinct()
        return documents

    # search for the documents using string, which is the title
    def search_doc_title(self, name):
        documents = self.user_card.library.documents.filter(title__contains=name).distinct()
        return documents

    # search for the documents using string, which contains keywords
    def search_doc_keywords(self, string):
        words = re.split('[ ,.+;:]+', string)
        documents = self.user_card.library.documents.filter(keywords__word__in=words).distinct()
        return documents

    # check out some copy of the document. If it is not possible returns False
    def check_out_doc(self, document):
        for copy in self.user_card.copies.all():
            if copy.document == document:
                return False  # user has already checked this document
        queue = document.queue_type(doc=document, user=self)
        if document.studentsQueue.count() + document.TAsQueue.count() + document.instructorsQueue.count() + document.visitingProfessorsQueue.count() + document.professorsQueue.count() > 0:
            queue.add(self)
            queue.model.save()
        for copy in document.copies.all():
            if not copy.is_checked_out:
                for doc in self.available_documents.objects.all():
                    if document.id == doc.id:
                        copy.is_checked_out = True
                        self.user_card.copies.add(copy)
                        copy.booking_date = datetime.date.today()
                        queue.exclude(user_card=self.user_card)
                        queue.model.save()
                        self.user_card.save()
                        copy.save()
                        return True
                CheckOutRequest.objects.create(user_card=self.user_card, copy=copy)
                return True
        return False  # there are no available copies

    # return copy of the document to the library. If it is not possible returns False
    def return_doc(self, document):
        for copy in self.user_card.copies.all():
            if copy.document == document:
                HandOverRequest.objects.create(user_card=self.user_card, copy=copy)
                self.user_card.copies.exclude(copy)
                self.user_card.save()
                Librarian.handed_over_copies.add(copy)
                return True
        return False  # no such document

    def has_overdue(self):  # bool
        for copy in self.user_card.copies.all():
            if copy.overdue_date > datetime.date:
                return True
        return False

    def type(self):
        if isinstance(self, Student):
            return "Student"
        if isinstance(self, Instructor):
            return "Instructor"
        if isinstance(self, TA):
            return "TA"
        if isinstance(self, Professor):
            return "Professor"
        if isinstance(self, VisitingProfessor):
            return "VisitingProfessor"


class Student(Patron):
    pass


class Faculty(Patron):
    def faculty_card(self, login, password, name):
        new_fac = Faculty()
        new_fac.login = login
        new_fac.password = password
        new_fac.name = name


class VisitingProfessor(Patron):
    pass


class Instructor(Faculty):
    pass


class TA(Faculty):
    pass


class Professor(Faculty):
    pass
